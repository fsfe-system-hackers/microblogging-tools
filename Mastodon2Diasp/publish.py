#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# publish.py: post messages from the GNU Social RSS feed to Diaspora
#
# Copyright (C) 2015  Bjoern Schiessle <bjoern@schiessle.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import ConfigParser
import os.path
from Gs2Diasp import Gs2Diasp
from feedDiasp.feedDiasp.RSSParser import RSSParser


class Publisher:

    def __init__(self):
        self.ready = self.__readConfig()

    def __readConfig(self):
        """ Read configuration from settings.cfg

        :return: true if config could be read successfully
        _:rtype: bool
        """
        if not self.__configExists():
            return False

        config = ConfigParser.ConfigParser()
        config.read('settings.cfg')

        try:
            # read GNU Social settings
            self.rssFeed = config.get('GNUSocial', 'url')
            self.gsUserName = config.get('GNUSocial', 'username')
            # read Diaspora Settings
            self.pod = config.get('Diaspora', 'pod')
            self.username = config.get('Diaspora', 'username')
            self.password = config.get('Diaspora', 'password')
        except Exception as e:
            print 'corrupt config file. Please compare your settings.cfg with the settings.sample.cfg and add the missing parameters'
            return False
        return True

    def __configExists(self):
        """ Check if config file settings.cfg exists

        :return: true if config exists
        :rtype: bool
        """
        if not os.path.isfile('settings.cfg'):
            print 'Please create a settings.cfg file. As a template you can copy settings.sample.cfg and adjust the values'
            return False
        return True

    def publish(self):
        """ Publish RSS feed from GNU Social to Diaspora """
        if not self.ready:
            return False

        rss = RSSParser(url=self.rssFeed)
        try:
            bot = Gs2Diasp(parser=rss, pod=self.pod, username=self.username, password=self.password, db='posts.txt', gsUserName=self.gsUserName)
            bot.publish()
        except Exception as e:
            print 'Login failed, try again later'
            return False

if __name__ == "__main__":
    pub = Publisher()
    pub.publish()
