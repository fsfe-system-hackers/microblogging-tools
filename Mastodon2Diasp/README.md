# Gs2Diasp

Post RSS feeds from Mastodon to Diaspora.

# Usage / Installation

edit the settings.cfg and add the password for the diaspora account,
you can find it in the internal svn.

In order to run the script you need to set the PYTHONPATH to the
python directory in this root folder.

A cronjob entry looks something like this:

0 * * * * cd /path/to/this/directory; export PYTHONPATH="/path/to/this/directory/python"; python publish.py 1> /dev/null
