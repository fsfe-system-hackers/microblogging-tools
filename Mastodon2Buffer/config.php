<?php
/**
 * @copyright Copyright (c) 2017 Bjoern Schiessle <bjoern@schiessle.org>
 *
 * @license GNU GPL version 3 or any later version
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

$config = [
	/** Buffer API configuration */
	'bufferClientId' => '',
	'bufferClientSecret' => '',
	'bufferAccessToken' => '',
	/** Mastodon Feed */
	'atomFeed' => 'https://mastodon.social/@fsfe.atom',
	/** @var bool $skipReplays don't post toot's which start with a '@' */
	'skipReplays' => true,
	/** @var bool $skipReShares don't post re-shares */
	'skipReShares' => true,
       	/** gets added to every post together with a link to the original post */
	'disclaimer' => 'Originally published outside of this walled garden in the free & open web:',
];
