# Mastodon2Buffer

This is a small tool which posts your Mastodon feed to Buffer which
will then forward your messages to all the social networks configured in
Buffer and enabled by default.

## Setup

Edit the config.php and add the "Client ID", "Client Secret" and
"Access Token". You find this values in the internal svn.

## Run

Running the program is as easy as `php mastodon2buffer.php`. Setup a cronjob
to check your feed for new toot's regularly and forward them.

If you want to test your setup without sending anything to Buffer you can call
`php mastodon2buffer.php --dry-run`. This will output all the Atom feed entries 
which would have been send to Buffer otherwise.

To avoid that on first run all your old posts from Mastodon will be
send to Buffer you can call `php mastodon2buffer.php --skip`. This way
all existing Atom feed entries will be marked as processed but not
send to Buffer. Only toot's created after you run the command will be
send to Buffer.

`php mastodon2buffer.php --help` gives you a overview of all commandline
arguments.
