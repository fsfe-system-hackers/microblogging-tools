# Introduction

Feed2Toot allows us to forward our RSS feed to Mastodon. This
repository contain the config files.

# Installation

Feed2Toot need to be installed on the server by following this
documentation: https://feed2toot.readthedocs.io/en/latest

# Setup

Take the config files from here and add the client credentials
(seperate them with a newline) to the feed2toot_clientcred.txt file
and the user credentials to the feed2toot_usercred.txt. Both can be
found in the internal SVN repository

After the installation was succesfull you can test the setup by running:

feed2toot --dry-run -c feed2toot.ini 

To avoid that all the old RSS entries will be posted to Mastodon on
the first run you should populate the cash with all old RSS entries by
running following command once:

feed2toot -p -c feed2toot.ini

Now you can setup a cronjob to run feed2toot once a hour:

0 * * * * /usr/local/bin/feed2toot -c /path/to/config/feed2toot.ini
