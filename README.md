# Microblogging Tools

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/microblogging-tools/00_README)

This repository contains a set of tools for the FSFE's microblogging activities.

* [Feed2Toot](https://git.fsfe.org/fsfe-system-hackers/microblogging-tools/src/branch/master/Feed2Toot)
* [Mastodon2Buffer](https://git.fsfe.org/fsfe-system-hackers/microblogging-tools/src/branch/master/Mastodon2Buffer)
* [Mastodon2Diasp](https://git.fsfe.org/fsfe-system-hackers/microblogging-tools/src/branch/master/Mastodon2Diasp)

## Feed2Toot

Feed2Toot allows us to forward our RSS feed to Mastodon. This
repository contain the config files.

## Mastodon2Buffer

This is a small tool which posts your Mastodon feed to Buffer which
will then forward your messages to all the social networks configured in
Buffer and enabled by default.

## Mastodon2Diasp

Post RSS feeds from Mastodon to Diaspora.


